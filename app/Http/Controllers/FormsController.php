<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SubscriberRequest;
use App\Http\Requests\SpeackerRequest;
use App\Http\Requests\SponsorRequest;
use App\Models\Subscriber;
use App\Models\Speacker;
use App\Models\Sponsor;

class FormsController extends Controller
{
    public function __construct()
    {

    }

	public function emailSubscribe(SubscriberRequest $request, Subscriber $model)
	{
		$model->create(['email' => $request->get('email')]);
		return redirect()->back()->withErrors(['success' => 'Thank you for subscribing']);
	}

	public function speakerSubscribe(SpeackerRequest $request, Speacker $model)
	{
		$model->create($request->all());
		return redirect()->back()->withErrors(['success' => 'Thank you for your submission. We will contact you soon']);
	}

	public function sponsorSubscribe(SponsorRequest $request, Sponsor $model)
	{
		$model->create($request->all());
		return redirect()->back()->withErrors(['success' => 'Thank you for your submission. We will contact you soon']);
	}

}
