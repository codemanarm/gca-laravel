<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SpeackerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'company' => 'max:255',
            'profession' => 'required|max:255',
            'topic' => 'required|max:255',
            'email' => 'email|required|max:255',
            'phone' => 'required|max:255',
        ];
    }
}
