<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Speacker extends Model
{
    protected $fillable = [
    	'type',
	    'name',
	    'surname',
	    'company',
	    'profession',
	    'topic',
	    'email',
	    'phone'
	];
}
