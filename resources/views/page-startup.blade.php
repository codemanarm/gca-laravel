@extends('layouts.app', ['body_class' => 'startup_page'])
@section('content')
    @if($errors->any())
        <div class="allert success email-subscribe-form">
            <p>{!! $errors->first() !!}</p>
        </div>
    @endif
    <div id="sponsors_container" class="wrapper startup-wrapper">
        <h1 class="white_title">STARTUP COMPETITIONS</h1>
        <span class=" text_center white_title startup-title">Apply for startup competitions</span>
        <div class="container">
            <div class="gca--form-submission-cover gca--form-submission-cover-startup"></div>
            <form action="{!! route('speaker-subscribe') !!}" class="gca--form-submission" id="startup-subscribe-form" method="POST">
                @csrf
                <input type="hidden" name="type" value="startup">
                <div class="gca--input-wrapper gca--input-wrapper-short">
                    <input name="name" type="text" class="gca--form-submission-input input-reset" placeholder="Name" required>
                </div>
                <div class="gca--input-wrapper gca--input-wrapper-short">
                    <input name="surname" type="text" class="gca--form-submission-input input-reset" placeholder="Surname" required>
                </div>
                <div class="gca--input-wrapper gca--input-wrapper-short">
                    <input name="company" type="text" class="gca--form-submission-input input-reset" placeholder="Comapany Name">
                </div>
                <div class="gca--input-wrapper gca--input-wrapper-short">
                    <input name="profession" type="text" class="gca--form-submission-input input-reset" placeholder="Profession">
                </div>
                <div class="gca--input-wrapper gca--input-wrapper-short">
                    <input name="topic" type="text" class="gca--form-submission-input input-reset" placeholder="Tell us about Your Project">
                </div>
                <div class="gca--input-wrapper gca--input-wrapper-short">
                    <input name="email" type="email" pattern="/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="gca--form-submission-input input-reset" placeholder="Email address">
                </div>
                <div class="gca--input-wrapper gca--input-wrapper-short">
                    <input name="phone" type="tel"  class="gca--form-submission-input input-reset" placeholder="Phone (+374)">
                </div>

                <button type="submit" class="gca--form-submission-button gca--button_submit">sign your startup</button>
            </form>
        </div>
    </div>

@endsection

@section('scripts')


<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\SpeackerRequest', '#startup-subscribe-form') !!}

@endsection