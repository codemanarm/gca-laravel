<div class="header">
    <a class="header_logo" href="{{ urlLang('/', 'en') }}"></a>
    <nav class="header_nav">
    	@if(isset($menu) && !empty($menu) && isset($menu->menuItems) && !empty($menu->menuItems))
    	    @foreach($menu->menuItems as $key => $item)
                @if(request()->url() == $item->link)
    	    	    <a data-navigation="{!! $item->id !!}" class="header_nav_item header_nav_item-active" href="{{ urlLang($item->link, 'en') }}">{{ $item->label }}</a>
                @else
                    <a data-navigation="{!! $item->id !!}" class="header_nav_item" href="{{ urlLang($item->link, 'en') }}">{{ $item->label }}</a>
                @endif
    	    @endforeach
    	@endif
       {{--  
        <a data-navigation="schedule" class="header_nav_item" href="/schedule.html">SCHEDULE</a>
        <a data-navigation="about" class="header_nav_item" href="/about.html">ABOUT US</a>
        <a data-navigation="speakers" class="header_nav_item" href="/speakers.html">SPEAKERS</a>
        <a data-navigation="sponsorsandpartners" class="header_nav_item" href="/sponsorsandpartners.html">SPONSORS & PARTNERS</a>
        <a data-navigation="contact" class="header_nav_item" href="/contact.html">CONTACT US</a> --}}
    </nav>
    <div class="burger_container">
        <div class="burger_bar_container">
            <div class="bar1 bars"></div>
            <div class="bar2 bars"></div>
            <div class="bar3 bars"></div>
        </div>   
        <div class="content">
            @if(isset($menu) && !empty($menu) && isset($menu->menuItems) && !empty($menu->menuItems))
                @foreach($menu->menuItems as $key => $item)
                    @if(request()->url() == $item->link)
                        <a data-navigation="{!! $item->id !!}" class="header_nav_item header_nav_item-active" href="{{ urlLang($item->link, 'en') }}">{{ $item->label }}</a>
                    @else
                        <a data-navigation="{!! $item->id !!}" class="header_nav_item" href="{{ urlLang($item->link, 'en') }}">{{ $item->label }}</a>
                    @endif
                @endforeach
            @endif
        {{--  
            <a data-navigation="schedule" class="header_nav_item" href="/schedule.html">SCHEDULE</a>
            <a data-navigation="about" class="header_nav_item" href="/about.html">ABOUT US</a>
            <a data-navigation="speakers" class="header_nav_item" href="/speakers.html">SPEAKERS</a>
            <a data-navigation="sponsorsandpartners" class="header_nav_item" href="/sponsorsandpartners.html">SPONSORS & PARTNERS</a>
            <a data-navigation="contact" class="header_nav_item" href="/contact.html">CONTACT US</a> --}}
        </div>
    </div>
</div>