<div class="overlay"></div>
<footer>
    <div class="powered_by">
        <span style="font-size: 12px; padding-left: 106px; margin-bottom: 3px;">Powered by</span>
        <a href="https://codeman.am/" target="_blank">
            <svg id="Layer_1" style=" width: 200px; height: 16px; display: block;" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 235.03 28.84">
                <defs><style>.cls-1{fill:#ffffff;} polygon{fill:#02a0de;}</style></defs>
                <title>codemanss-logo</title>
                <g id="Layer_1-2" data-name="Layer_1">
                    <path class="cls-1" d="M401.57,395.49H398c-3.43-9.09-4.91-13.15-8.41-22.65a11.58,11.58,0,0,1-.42-1.87H389a10.49,10.49,0,0,1-.44,1.87c-3.35,9.16-5.09,13.85-8.22,22.65h-3.63l10.7-27.91h3.39l10.74,27.91h0Z" transform="translate(-200.89 -367.11)"></path><path class="cls-1" d="M272.88,395.49V367.58h7.71q14.75,0,14.75,13.6a13.72,13.72,0,0,1-4.1,10.38q-4.1,3.92-11,3.92h-7.39Zm3.27-24.95v22h4.17q5.49,0,8.54-2.94t3.06-8.33q0-10.72-11.41-10.72h-4.36Z" transform="translate(-200.89 -367.11)"></path>
                    <path class="cls-1" d="M246.35,396a12.23,12.23,0,0,1-9.51-3.91,14.51,14.51,0,0,1-3.57-10.18q0-6.74,3.64-10.74t9.91-4a12,12,0,0,1,9.31,3.89,14.56,14.56,0,0,1,3.53,10.18q0,6.83-3.62,10.8a12.47,12.47,0,0,1-9.69,4h0Zm0.23-25.88a9,9,0,0,0-7.14,3.17,14.11,14.11,0,0,0-.07,16.63,8.73,8.73,0,0,0,7,3.14,9.16,9.16,0,0,0,7.24-3q2.65-3,2.64-8.39t-2.56-8.54a8.81,8.81,0,0,0-7.09-3h0Z" transform="translate(-200.89 -367.11)"></path>
                    <path class="cls-1" d="M435.91,395.49h-4l-14.36-22.25a9.28,9.28,0,0,1-.9-1.75h-0.11a29.6,29.6,0,0,1,.15,3.83v20.16h-3.27V367.58h4.24l14,21.89c0.58,0.9,1,1.53,1.13,1.87h0.08a28.8,28.8,0,0,1-.2-4.11V367.58h3.27v27.91h0Z" transform="translate(-200.89 -367.11)"></path>
                    <path class="cls-1" d="M364.9,395.49h-3.25V376.77c0-1.48.09-3.29,0.27-5.43h-0.08A17.27,17.27,0,0,1,361,374l-9.53,21.45h-1.6l-9.51-21.29a16.7,16.7,0,0,1-.84-2.86h-0.08c0.11,1.12.15,2.94,0.15,5.47v18.69h-3.15V367.58h4.32L349.33,387a25.71,25.71,0,0,1,1.29,3.35h0.11c0.56-1.53,1-2.67,1.34-3.42l8.74-19.38h4.09v27.91h0Z" transform="translate(-200.89 -367.11)"></path>
                    <polygon points="107.67 15.65 107.67 12.71 110.94 12.71 122.46 12.71 122.46 15.65 110.94 15.65 107.67 15.65"></polygon>
                    <polygon points="122.46 28.38 107.67 28.38 107.67 25.42 110.94 25.42 121.03 25.42 122.46 25.42 122.46 28.38"></polygon>
                    <path class="cls-1" d="M214,396a12.23,12.23,0,0,1-9.51-3.91,14.51,14.51,0,0,1-3.57-10.18q0-6.74,3.64-10.74t9.91-4a12,12,0,0,1,8.88,3.44L221,372.82a8.87,8.87,0,0,0-6.85-2.75,9,9,0,0,0-7.14,3.17,14.12,14.12,0,0,0-.07,16.63,8.73,8.73,0,0,0,7,3.14,9.21,9.21,0,0,0,7.06-2.8l2.21,2.21A12.57,12.57,0,0,1,214,396h0Z" transform="translate(-200.89 -367.11)"></path>
                    <polygon points="107.67 3.41 107.67 0.47 110.94 0.47 122.46 0.47 122.46 3.41 110.94 3.41 107.67 3.41"></polygon>
                </g>
            </svg>
        </a>
    </div>
</footer>