@php
    $current_lang = LaravelLocalization::getCurrentLocale();
    $menu = Codeman\Admin\Menu\Models\Menus::getMenuWithItems('Main Menu', $current_lang);
    $default_lang = $settings['default_lang'];
    if(isJson($settings['all_langs'])){
        $languages = json_decode($settings['all_langs']);
    }
    $body_class = isset($body_class) ? $body_class : '';
@endphp
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,400,700,900&display=swap"
            rel="stylesheet">
        <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
        <link rel="stylesheet" href="/gca/style/main.css">
        <title>@yield('page-title')</title>
    </head>
    <body class="{!! $body_class !!}">
    
        @include('layouts.parts.header')
            <main id="" class="transition-fade m-auto">
                @yield('content')
            </main>
        @include('layouts.parts.footer')

    </body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
            
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.1.1/gsap.min.js"></script>
    <script src="https://unpkg.com/delaunator@4.0.1/delaunator.min.js"></script>
    <script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js"></script>
    @yield('scripts')
    

    <script src="/gca/js/pageTransition.js"></script>
    
</html>