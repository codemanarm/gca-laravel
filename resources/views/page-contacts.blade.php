@extends('layouts.app', ['body_class' => 'contact'])
@section('content')
    @if($errors->any())
        <div class="allert success email-subscribe-form">
            <p>{!! $errors->first() !!}</p>
        </div>
    @endif
    <div class="contact_cover">
        <h1 class="title_big white_title">meet us here</h1>
        <div class="container contact_map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3047.537479066553!2d44.516349415201994!3d40.197105576891495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x406abd24fb2027b3%3A0x4aadd2fd2e9ca1f2!2sRadisson%20Blu%20Hotel%2C%20Yerevan!5e0!3m2!1sen!2s!4v1583925877841!5m2!1sen!2s" width="100%" height="584" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </div>
    <div class="container contact-info">
        <div class="contact-item contact-mob">
            <i class="contact-mob-icon"></i>
            <span class="contact-name">phone</span>
            <a href="tel:+37455107272" class="contact-data">+ 374 55 10 72 72</a>
        </div>
        <div class="contact-item contact-location">
            <i class="contact-mob-marker"></i>
            <span class="contact-name" >address</span>
            <span class="contact-data">Radisson Blu Hotel</span>
            <span class="contact-data">Azatutyan Avenue 2/2, 0037</span>
            <span class="contact-data">Yerevan Armenia</span>
        </div>
        <div class="contact-item contact-mail">
            <i class="contact-mob-email"></i>
            <span class="contact-name">e-mail</span>
            <a class="contact-data" href="mailto: info@gameconf.am">info@gameconf.am</a>
        </div>
    </div>
    <div class="footer">
        <h2 class="title_center white_title">GET THE LATEST INFO ABOUT GCA</h2>
        <form action="{{ route('email-subscribe') }}" method="POST" id="email-subscribe-form">
            @csrf 
            <div class="gca--input-wrapper">
                <input class="inp" type="email" name="email" placeholder="Email :">
            </div>
            <button class="footer_btn  gca--button_submit" type="submit">subscribe</button>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\SubscriberRequest', '#email-subscribe-form') !!}
@endsection