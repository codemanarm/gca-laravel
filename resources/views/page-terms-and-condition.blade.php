@extends('layouts.app')
@section('content')
    <div class="terms_and_conditions">
        <h1 class="privacy_page_title">Terms and Conditions</h1>
        <span class="privacy_desc mb-24">Please read the following Terms and Conditions carefully before signing the document. If you do not agree to these Terms and Conditions, please do not sign this document.</span>
        <h1 class="privacy_title">General Terms</h1>
        
        <h2 class="privacy_sub_header">Copyright</h2>
        <span class="mb-30">
            The entire content included in this document is copyrighted as a collective work of the Armenian and other copyright laws and is the property of the GCA Ltd. & Game Conference Armenia.
        </span>
        <span class="mb-30">
            Any use, including but not limited to the reproduction, distribution, display or transmission of the content of this document is strictly prohibited unless authorized by the organizers.
        </span>

        <h2 class="privacy_sub_header">Changes or cancellation of GCA20</h2>
        <span class="mb-30">
            We try to make sure that the Conference programs, speakers, topics and the venue are correct at the time of publishing, circumstances beyond our control may necessitate substitutions, alterations, postponements, or cancellations to the content, format, themes, name, performers, hosts, moderators, venue, or timing of the Conference.
        </span>
        <span class="mb-30">
            We will endeavor to notify you as soon as reasonably practicable of any substitutions, postponements, or changes by posting the updated information on GCA20 <a href="https://gameconf.am/">website</a>
        </span>
        <span class="mb-30">
            In the unlikely event of postponement or cancellation of the Conference, our total aggregate liability to you is limited to the refund of paid fees that remain after credit card and payment processing fees have been incurred and deducted, and we will not be liable to you for any expenditure, damage or loss incurred by you as a result of the cancellation or postponement.
        </span>
        <h2 class="privacy_sub_header">Photography, audio, and video recording</h2>
        <span class="mb-30">
            By attending the Conference you acknowledge and agree that the Conference (or any part of it) may be photographed or recorded by us or our partners. You agree to permit us, or any third party licensed by us, to use, distribute, broadcast, or otherwise globally disseminate your likeness, name, voice and words in perpetuity in television, radio, film, newspapers, magazines and other media now available and hereafter developed, both before, during and any time after the Conference, and in any form, without any further approval from you or any payment to you. This grant includes, but is not limited to, the right to edit the media, the right to use the media (alone or together with other information), and the right to allow others to use or distribute the media.
        </span>

        <h1 class="privacy_title">Attendees</h1>
        <h2 class="privacy_sub_header">Tickets and pricing</h2>
        <span class="mb-30">
            You will find details of attendee ticket pricing and fees for the Conference <a href="/get-ticket.html">​here​</a>. Ticket prices for GCA are correct at the time of publication
        </span>
        <span class="mb-30">
            We reserve the right to change the ticket prices at any time but any changes will not affect tickets that have already been purchased.
        </span>
        <span class="mb-30">
            A valid ticket grants you access to all of our sessions between April 18-19, as well as the exhibition and networking areas of the Conference.
        </span>
        <h3 class="mb-30">
            Here are some more great opportunities for those attending GCA20:
        </h3>
        <ul>
            <li>
                Networking opportunities with gaming industry veterans during the conference
            </li>
            <li>
                Free access to GCA20 Gaming Corner
            </li>
        </ul>
        <h2 class="privacy_sub_header">Cancellation</h2>
        <span class="mb-30">
            Fees paid are non-refundable. However, participation by a substitute will be allowed until October 1, 2019.
             Thereafter change of the name on the ticket will be prohibited.
              If you do not receive a confirmation email after submitting payment information,
               or if you experience an error message or service interruption after submitting payment information, please get in touch with us at​ <a href="mailto:info@gameconf.am">info@gameconf.am</a> to confirm whether the registration has been done or not. We don’t want you to lose money in case you assume that registration has not been done because you failed to receive confirmation.
        </span>
        <h3 class="mb-30">
            Cancellation and refunds
        </h3>
        <ul>
            <li>
                Registration once done cannot be canceled or refund offline
            </li>
            <li>
                Partial cancellation of a registration is not possible
            </li>
            <li>
                On any registration, the customer will receive a registration confirmation email. Please note that the registration confirmation is NOT the ticket. In early April You will get new email with your ticket information inside.
            </li>
        </ul>
        <h2 class="privacy_sub_header">Payment methods</h2>
        <span class="mb-30">
            We accept Visa, MasterCard, American Express, Debit Cards, Net Banking. Also you can buy your ticket using Telcell terminals.
        </span>
        <h2 class="privacy_sub_header">Pricing, availability and registration confirmation</h2>
        <span class="mb-30">
            GCA20 organizers have no control over the availability of seats. After the payment is complete, you will be given a chance to assign or will be assigned a ticket. However, participation by a substitute will be allowed until April 14, 2020. Thereafter change of the name on the ticket will be prohibited.
        </span>
        <span>
            If an event is canceled or postponed and the organizers of the event agree on issuing a refund for the registration, it is up to the organizers to contact you and process the refund.
        </span>
        <h2 class="privacy_sub_header">Admission</h2>
        <ul>
            <li>Entry Ticket</li>
            <li>
                Email confirmation: Your registration confirmation will be sent via an email. You can find your invoice in your ticket confirmation email. You should receive this email after you have purchased the ticket.
            </li>
        </ul>
        <h1 class="privacy_page_title">Changes to the GCA20 Terms and Conditions</h1>
        <span>Any changes to this Terms and Conditions will be posted on this page. The updated Terms and Conditions will take effect as soon as it has been updated.</span>
    </div>
@endsection        
