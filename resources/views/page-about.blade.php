@extends('layouts.app', ['body_class' => 'about_page'])
@section('content')

    <div class="container" data-router-view="about">
        <h1 class="white_title title_big">organizers</h1>
        <div class="about_cover">
            <img src="/gca/assets/about.png" alt="">
        </div>
        <div class="about_description">
            <div class="about_us_wrapper">
                <div class="wrapper_title">
                    <i class="gca--information-icon gca--icon"></i>
                    <h1 class="white_title">About us</h1>
                </div>
                <span>
                    <b>The Game Conference Armenia (GCA)</b> is the first ever event in Armenia to bring the game development community together on a massive scale.
                     With the rising tide of the game development interest in Armenia we can’t wait to welcome and learn from industry veteran game designers, artists,
                     programmers, audio professionals and producers. Key features of GCA are :
                     <ul class="about_us_list">
                         <li>Selection of lectures from <b>Industry Veterans</b></li>
                         <li>Variety of lectures from <b>inhouse game developers</b></li>
                         <li><b>Dedicated space</b> where <b>GCA</b> attendees can take a break , meet new people and relax. </li>
                     </ul>
                     <b>GCA will also have a <b>Game Startup Competition / Showcase</b> where startups will have a chance to show their product on stage and off stage on the showcase floor .</b>
                </span>
            </div>
            <div class="our_mission">
                <div class="wrapper_title">
                    <i class="gca--icon gca--focus-icon"></i>
                    <h1 class="white_title">our mission</h1>
                </div>
                <span>
                    We have one mission in mind : To become a pillar for all game developers in the region.
                    Provide learning and networking opportunities, to share experience and get inspired by Industry Veterans to make games and stories
                     for everyone in the world to enjoy.
                </span>
            </div>
        </div>
        @if(isset($page->team) && !empty($page->team))
        <div class="staff_container">
            <div class="wrapper_title">
                <i class="fas fa-users gca--icon color-white"></i>
                <h1 class="white_title">Staff Bio</h1>
            </div>
            @foreach($page->team as $resource)
            <div class="staff_item">
                <div class="staff_member_cover">
                    <img src="{{ image_thumbnail($resource->thumbnail, 300, 430) }}" alt="{{$resource->title}}">
                    <div class="staff-credentail">
                        <h2 class="staff_member_name">{{$resource->title}}</h2>
                        <span class="staff_member_profession">
                            @if(isset($resource->meta) && isset($resource->meta['position']))
                                {{ $resource->meta['position'] }}
                            @endif
                        </span>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endif
    </div>
@endsection