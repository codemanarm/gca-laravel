@extends('layouts.app', ['body_class' => 'schedule_page'])
@section('content')
    <div class="schedule_dates_container">
            <div class="schedule_dates">
                <a href="" class="schedule_date_item active">
                    <span class="schedule_date_day">18</span> 
                    <span class="schedule_date_month">APR
                         <span class="schedule_date_week_day">SAT</span>
                    </span>
                </a>
                <a href="" class="schedule_date_item pointer-none">
                    <span class="schedule_date_day">19</span> 
                    <span class="schedule_date_month">APR
                         <span class="schedule_date_week_day">SUN</span>
                    </span>
                </a>
            </div>
            <div class="schedule_types">
                <div class="schedule_type schedule_type_art">
                    <span></span>
                    Art
                </div>
                <div class="schedule_type schedule_type_design">
                    <span></span>
                    Game Design
                </div>
                <div class="schedule_type schedule_type_development">
                    <span></span>
                    Development
                </div>
                <div class="schedule_type schedule_type_other">
                    <span></span>
                    Other
                </div>
            </div>
    </div>
   <!-- schedule -->
    <div class="schedule">
        <div class="schedule_hall">
            <div class="schedule_hall_item">Hall 1</div>
            <div class="schedule_hall_item">Hall 2</div>
            <div class="schedule_hall_item">Hall 3</div>
        </div>
    
        <div class="schedule_row_wrapper">
            <div class="schedule_time">
                <div class="schedule_time_hour">09:30</div>
                <div class="schedule_time_hour">10:00</div>
                <!-- <div class="schedule_time_hour">11:00</div>
                <div class="schedule_time_hour">12:00</div>
                <div class="schedule_time_hour">13:00</div>
                <div class="schedule_time_hour">14:00</div>
                <div class="schedule_time_hour">15:00</div>
                <div class="schedule_time_hour">16:00</div>
                <div class="schedule_time_hour">17:00</div>
                <div class="schedule_time_hour">18:00</div> -->
            </div>
            <div class="schedule_row">
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
            </div>
            <div class="schedule_row">
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
            </div>
            <div class="schedule_row">
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
            </div>
          
        </div>
        <div class="schedule_row_wrapper_mob">
            <div class="schedule_row_mob">
                <div class="schedule_time_hour">09:30</div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
            </div>
            <div class="schedule_row_mob">
                <div class="schedule_time_hour">09:30</div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
            </div>
            <div class="schedule_row_mob">
                <div class="schedule_time_hour">09:30</div>
                <div class="schedule_row_hour"></div>
            </div>
            <div class="schedule_row_mob">
                <div class="schedule_time_hour">09:30</div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
                <div class="schedule_row_hour"></div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="/gca/js/schedule.js"></script>
@endsection