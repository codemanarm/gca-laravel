@extends('layouts.app', ['body_class' => 'speakers_page'])
@section('content')
    @if($errors->any())
        <div class="allert success email-subscribe-form">
            <p>{!! $errors->first() !!}</p>
        </div>
    @endif
    <div class="speakers-container">
        <div class="speaker-single-custom">
            <h1 class="speaker-name">?</h1>
        </div>
    </div>
    <div class="overlay">
        <a href="#" class="close_button">
            <i class="fas fa-times"></i>
        </a>
    </div>
    <div class="gca--form-submission-wrapper container">
        <div class="gca--form-submission-cover gca--form-submission-cover-speaker"></div>
        <form action="{!! route('speaker-subscribe') !!}" class="gca--form-submission" id="speaker-subscribe-form" method="POST">
            @csrf
            <input type="hidden" name="type" value="speaker">
            <span class="gca_font_sf_thin">Apply to speak at the conference</span>
            <div class="gca--input-wrapper gca--input-wrapper-short">
                <input name="name" type="text" class="gca--form-submission-input input-reset" placeholder="Name" required>
            </div>
            <div class="gca--input-wrapper gca--input-wrapper-short">
                <input name="surname" type="text" class="gca--form-submission-input input-reset" placeholder="Surname" required>
            </div>
            <div class="gca--input-wrapper gca--input-wrapper-short">
                <input name="company" type="text" class="gca--form-submission-input input-reset" placeholder="Comapany Name">
            </div>
            <div class="gca--input-wrapper gca--input-wrapper-short">
                <input name="profession" type="text" class="gca--form-submission-input input-reset" placeholder="Profession">
            </div>
            <div class="gca--input-wrapper gca--input-wrapper-short">
                <input name="topic" type="text" class="gca--form-submission-input input-reset" placeholder="Tell us about Your Topic">
            </div>
            <div class="gca--input-wrapper gca--input-wrapper-short">
                <input name="email" type="email" pattern="/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="gca--form-submission-input input-reset" placeholder="Email address">
            </div>
            <div class="gca--input-wrapper gca--input-wrapper-short">
                <input name="phone" type="tel"  class="gca--form-submission-input input-reset" placeholder="Phone (+374)">
            </div>
            <button type="submit" class="gca--form-submission-button gca--button_submit">join us</button>
        </form>
    </div>

@endsection

@section('scripts')
<script src="/gca/js/speakers.js"></script>
<script>
    const speakersData = [];
    @if(isset($page->speakers) && !empty($page->speakers))
        @foreach($page->speakers as $key => $resource)
            @php
                $host = false;
                if($key == 0):
                    $host = true;
                endif;
            @endphp
            speakersData.push({
                id: '{!! $resource->id !!}',
                host: '{!! $host !!}',
                name: '{!! $resource->title !!}',
                title: '{!! $resource->meta['position'] !!}',
                separatedTitle: '{!! $resource->meta['separated_title'] !!}',
                profession: '{!! $resource->meta['profession'] !!}',
                time: '{!! $resource->meta['pitch_time'] !!}',
                pitchDate: '{!! $resource->meta['pitch_date'] !!}',
                place: '{!! $resource->meta['place'] !!}',
                imageUrl: '{!! image_thumbnail($resource->thumbnail, 564, 744) !!}'
            }); 
        @endforeach
    @endif
    initData(speakersData)
</script>

<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\SpeackerRequest', '#speaker-subscribe-form') !!}

@endsection