@extends('layouts.app', ['body_class' => 'home'])
@section('content')
    <div id="app"></div>
    <div id="home_container" class="wrapper">
        @if($errors->any())
            <div class="allert success email-subscribe-form">
                <p>{!! $errors->first() !!}</p>
            </div>
        @endif
        <!-- hero -->
        <div class="hero">
            <div class="hero_content">
                <div class="hero_content_left_side">
                    <div>April 18-19</div>
                    <div>Radisson Blu Hotel</div>

                </div>
                <div class="hero_content_right_side">
                    <canvas id="canvas-webgl" class="p-canvas-webgl" ></canvas>
                </div>
            </div>
            <button id="eventbrite-widget-modal-trigger-96324078913"  class="hero_btn gca--button_submit gca--button_get-ticket" type="button">Buy Tickets</button>
        </div>
        <!-- hero end-->

        <!-- about -->
        <div class="about">
            <div class="about_image">
                <div></div>
            </div>
            <div class="about_text">
                <h2 class="white_title">ABOUT US</h2>
                <p>
                    <strong>The Game Conference Armenia</strong>
                    (GCA) is the first ever event in Armenia to bring game development community
                    together on a massive scale. With the rising tide of the game development
                    interest in Armenia we can’t wait to welcome and learn from industry veteran
                    game designers, artists, programmers, audio professionals and producers.</p>

                <p>Key features of
                    <strong>GCA</strong>
                    are : Selection of lectures from
                    <strong>Industry Veterans,</strong>
                    Variety of lectures from inhouse
                    <strong>game developers,</strong>
                    dedicated space where GCA attendees , can take a break, meet
                    <strong>new people</strong>
                    and relax.
                </p>

                <p>
                    GCA will also have Game
                    <strong>Startups</strong>
                    Competition</p>
                <a href="{{ urlLang('/about-us', 'en') }}" class="white_btn" >read our story</a>
            </div>

        </div>
        <!-- about end -->

        <!-- speakers -->
        @if(isset($page->speakers) && !empty($page->speakers))
                <h2 class="title_center white_title mb-0-md">MEET OUR SPEAKERS</h2>
                <div class="speakers swiper-container">
                    <div class="speakers_wrapper swiper-wrapper">
                        @foreach($page->speakers as $key => $resource)
                        <div class="swiper-slide">
                            <div class="speaker">
                                <div class="speaker_img speaker_img_3" style="background-image: url('{{ $resource->thumbnail }}')"></div>
                                <div class="speaker_text">
                                    <div class="speaker_text_name">{{ $resource->title }}</div> 
                                    <div class="speaker_text_position">{!! $resource->meta['position'] !!}</div>
                                    <div class="speaker_text_position speaker_text_position_glow">{!! $resource->meta['profession'] !!}</div>
                                    <p>{!! $resource->meta['separated_title'] !!}</p>
                                    <a href="{{ urlLang('/speakers', 'en') }}" class="white_btn">see more</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
        @endif

        <!-- speakers end -->

        <!-- sponsors -->
        <!-- <div class="sponsors">
            <h2 class="title_center white_title">MEET OUR SPONSORS</h2>
            <div class="sponsor_heading sponsor_heading_platinum">platinum</div>
            <div class="sponsors_wrapper">
                <div class="sponsor">
                    <div class="sponsor_item sponsor_item-autodesk sponsor_item-platinum">
                        <div></div>
                    </div>
                    <a href="{{ urlLang('/sponsors-and-partners', 'en') }}" class="white_btn">see more</a>
                </div>
            </div>

            <div class="sponsor_heading sponsor_heading_gold">Gold</div>
            <div class="sponsors_wrapper">
                <div class="sponsor">
                    <div class="sponsor_item sponsor_item-fris">
                        <div></div>
                    </div>
                    <a href="{{ urlLang('/sponsors-and-partners', 'en') }}" class="white_btn">see more</a>
                </div>

                <div class="sponsor">
                    <div class="sponsor_item sponsor_item-ucom">
                        <div></div>
                    </div>
                    <a href="{{ urlLang('/sponsors-and-partners', 'en') }}" class="white_btn">see more</a>
                </div>
            </div>
        </div> -->
        <!-- sponsors end -->

        <!-- footer -->
        <div class="footer">
            <h2 class="title_center white_title">GET THE LATEST INFO ABOUT GCA</h2>
            
            <form action="{{ route('email-subscribe') }}" method="POST" id="email-subscribe-form">
                @csrf
                <div class="gca--input-wrapper">
                    <input class="inp" type="email" name="email" placeholder="Email :">
                    
                </div>
                <button class="footer_btn  gca--button_submit" type="submit">subscribe</button>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <noscript><a href="https://www.eventbrite.com/e/game-conference-armenia-2020-tickets-96324078913" rel="noopener noreferrer" target="_blank"></noscript>
    <noscript></a>Buy Tickets on Eventbrite</noscript>
    <script src="https://www.eventbrite.com/static/widgets/eb_widgets.js"></script>
    <script type="text/javascript">
        var exampleCallback = function() {
            console.log('Order complete!');
        };
        window.EBWidgets.createWidget({
            widgetType: 'checkout',
            eventId: '96324078913',
            modal: true,
            modalTriggerElementId: 'eventbrite-widget-modal-trigger-96324078913',
            onOrderComplete: exampleCallback
        });
    </script>
    <script src="/gca/js/particleJS/particle.js"></script>
    <script src="/gca/js/particleJS/particle_chunk.js"></script>
    <script src="/gca/js/glitchJS/glitch.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\SubscriberRequest', '#email-subscribe-form') !!}
@endsection
