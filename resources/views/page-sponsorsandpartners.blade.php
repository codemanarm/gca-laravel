@extends('layouts.app', ['body_class' => 'sponsors_page'])
@section('content')
    @if($errors->any())
        <div class="allert success email-subscribe-form">
            <p>{!! $errors->first() !!}</p>
        </div>
    @endif
    <div id="sponsors_container" class="wrapper">
        <h1 class="white_title">Our Sponsors</h1>
        <button class="gca--button_submit hero_btn gca--open_form">become a sponsor</button>
        <div class="sponsors_grid">

            @if(isset($page->sponsors_categories) && !empty($page->sponsors_categories))
                @foreach($page->sponsors_categories as $cat_key => $category)
                    @php
                        $first_sponsor_item = 0
                    @endphp
                    @if(isset($page->sponsors) && !empty($page->sponsors))
                        @foreach($page->sponsors as $key => $resource)
                            @if($resource->categories[0]->id == $category->id && $category->title != 'Partner')
                                @if($first_sponsor_item == 0)
                                <div class="sponsor_{!! strtolower($category->title) !!}">
                                    <div class="sponsor_heading sponsor_heading_{!! strtolower($category->title) !!}">{!! $category->title !!}</div>
                                </div>
                                    @php
                                        $first_sponsor_item = 1;
                                    @endphp
                                @endif

                                <div class="container m-auto">
                                        <div class="sponsors_wrapper sponsor_{!! strtolower($category->title) !!}">
                                            <div class="sponsor">
                                                <div class="sponsor_item sponsor_item-{!! strtolower($resource->title) !!} sponsor_item-{!! strtolower($category->title) !!}">
                                                    <div style="background-image: url({!! $resource->thumbnail !!})"></div>
                                                </div>
                                            </div>
                                            <div class="sponsor_about">
                                                <h2 class="sponsor_name">{!! $resource->title !!}</h2>
                                                <div class="sponsor_bio">
                                                    {!! $resource->content !!}
                                                </div>
                                                @if(isset($resource->meta) && isset($resource->meta['website_url']))
                                                <a href="{{ $resource->meta['website_url'] }}" class="sponsor_link" target="_blank">{{ $resource->meta['website_url'] }}</a>
                                                @endif
                                            </div>
                                        </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endif

        </div>

        
        <div class="partners_section">
            <div class="container m-auto">
                <div class="partner_section_header">
                    <h1 class="white_title">Our Partners</h1>
                    <div class="gca--button_submit hero_btn gca--open_form">become a partner</div>
                </div>
                @php
                    $first_partner_item = 0
                @endphp
                @if(isset($page->sponsors) && !empty($page->sponsors))
                    @foreach($page->sponsors as $key => $resource)
                        @if($resource->categories[0]->title == 'Partner')
                            @if($first_partner_item == 0)
                                
                                @php
                                    $first_partner_item = 1;
                                @endphp

                            @endif
                                <div class="sponsors_wrapper sponsor_silver">
                                    <div class="sponsor">
                                        <div class="sponsor_item sponsor_{!! strtolower($resource->title) !!}">
                                            <div style="background-image: url({!! $resource->thumbnail !!})"></div>
                                        </div>
                                    </div>
                                    <div class="sponsor_about">
                                        <h2 class="sponsor_name">{!! $resource->title !!}</h2>
                                        <div class="sponsor_bio">
                                            {!! $resource->content !!}
                                        </div>
                                        @if(isset($resource->meta) && isset($resource->meta['website_url']))
                                            <a href="{{ $resource->meta['website_url'] }}" class="sponsor_link" target="_blank">{{ $resource->meta['website_url'] }}</a>
                                        @endif
                                    </div>
                                </div>
                        @endif
                    @endforeach
                @endif
                
            </div>
        </div>
    </div>
    <div id="gca--form-submissionSponsor" class="gca--form-submission-wrapper container align-items-center">
        <div class="gca--form-submission-cover gca--form-submission-cover-sponsor"></div>
        <form action="{{ route('sponsor-subscribe') }}" class="gca--form-submission gca--form-submission-sponsor" id="sponsor-form" method="POST">
            @csrf
            <input name="type" type="hidden"  value="sponsor">
            <span class="gca_font_sf_thin">Become a Sponsor</span>
            <div class="gca--input-wrapper">
                <input name="comapany" type="text" class="gca--form-submission-input input-reset" placeholder="Comapany Name">
            </div>
            <div class="gca--input-wrapper">
                <input name="name" type="text" class="gca--form-submission-input input-reset" placeholder="Contact Person Name">
            </div>
            <div class="gca--input-wrapper">
                <input name="email" type="email" pattern="/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="gca--form-submission-input input-reset" placeholder="Email address">
            </div>
            <div class="gca--input-wrapper">
                <input name="phone" type="tel"  class="gca--form-submission-input input-reset" placeholder="Phone (+374)">
            </div>
            <button type="submit" class="gca--form-submission-button gca--button_submit">join us</button>
        </form>
    </div>
    <div id="gca--form-submissionPartner" class="gca--form-submission-wrapper container align-items-center">
        <div class="gca--form-submission-cover gca--form-submission-cover-partner"></div>
        <form action="{{ route('sponsor-subscribe') }}" class="gca--form-submission gca--form-submission-partner" id="partner-form" method="POST">
            @csrf
            <input name="type" type="hidden"  value="partner">
            <span class="gca_font_sf_thin">Become a Partner</span>
            <div class="gca--input-wrapper">
                <input name="comapany" type="text" class="gca--form-submission-input input-reset" placeholder="Comapany Name">
            </div>
            <div class="gca--input-wrapper">
                <input name="name" type="text" class="gca--form-submission-input input-reset" placeholder="Contact Person Name">
            </div>
            <div class="gca--input-wrapper">
                <input name="email" type="email" pattern="/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="gca--form-submission-input input-reset" placeholder="Email address">
            </div>
            <div class="gca--input-wrapper">
                <input name="phone" type="tel"  class="gca--form-submission-input input-reset" placeholder="Phone (+374)">
            </div>
            <button type="submit" class="gca--form-submission-button gca--button_submit">join us</button>
        </form>
    </div>
    <div class="overlay">
        <a href="#" class="close_button">
            <i class="fas fa-times"></i>
        </a>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\SponsorRequest', '#partner-form') !!}
    {!! JsValidator::formRequest('App\Http\Requests\SponsorRequest', '#sponsor-form') !!}
@endsection



