<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('email-subscribe', 'FormsController@emailSubscribe')->name('email-subscribe');
Route::post('speaker-subscribe', 'FormsController@speakerSubscribe')->name('speaker-subscribe');
Route::post('sponsor-subscribe', 'FormsController@sponsorSubscribe')->name('sponsor-subscribe');
Route::post('startup-subscribe', 'FormsController@startupSubscribe')->name('startup-subscribe');

Route::group([
	'prefix' => LaravelLocalization::setLocale(), 
	'middleware' => ['web', 'language' ]], 
function() {
		

 
	

    Route::namespace('\Codeman\Admin\Http\Controllers\Front')->group(function () {
        Route::get('/language/{lang}', 'LanguagesController@changeLanguage');
        Route::get('/{slug?}', 'PagesController@index')->where('slug', '(?s).*');
    });


});
