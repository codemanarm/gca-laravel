const initData = (speakersData) => {
    
    function popupToggler(element,overlay,closeBtn){
        
        if(closeBtn){
            element.classList.remove('collapsed');
            overlay.classList.remove('show');
            document.body.classList.remove('collapsed');
            if(element.classList.contains('speaker-single') && window.innerWidth <1366){
                element.style.top = 0;
                element.style.transform = 'none';
            }
            return;
        }
        
        if(element.classList.contains('speaker-single') && window.innerWidth <1366){
            element.style.top = window.pageYOffset + 10 + 'px';
            element.style.transform = 'translateX(-50%)';
        }

        element.classList.add('collapsed');
        overlay.classList.add('show');
        document.body.classList.add('collapsed');
    }
    
    
    const speakersContainer = document.querySelector('.speakers-container');
    const speakerForm = document.querySelector('.gca--form-submission-wrapper');
    const overlay = document.querySelector('.overlay');
    const overlayCloseBtn = document.querySelector('.close_button');
    
    
    speakersData.map(item => {
    
        const {name, imageUrl, title, profession , host, place, pitchDate, time} = item;
    

        const becomeASpeakerBtn =document.createElement('div');
        becomeASpeakerBtn.className="gca--button_submit gca--button-customForm"
        becomeASpeakerBtn.innerText = 'Become a Speaker'

        becomeASpeakerBtn.addEventListener('click', () => {
            popupToggler(speakerForm, overlay)
        })

        const speakerSingle = document.createElement('div');
        speakerSingle.classList.add(`${host ? 'host-container': 'speaker-single'}`);
    
        !host && speakerSingle.addEventListener('click',function(e){
            e.preventDefault();
            popupToggler(this, overlay)
        })
    
        
        const speakerCardOverlay = document.createElement('img');
        speakerCardOverlay.src = 'gca/assets/traingle.png';
        speakerCardOverlay.alt = 'Card Overlay';
        speakerCardOverlay.classList.add('speaker-card-overlay');
    
        const speakerImage = document.createElement('img');
        speakerImage.src = `${imageUrl}`;
        speakerImage.alt = `${name}`;
        
    
        const speakerBio = document.createElement('div');
        speakerBio.classList.add(`${host && 'host-bio' || 'speaker-bio'}`);
    
    
        const hostAnimationTitle = document.createElement('h1');
        hostAnimationTitle.classList.add('text-reveal');
        hostAnimationTitle.innerHTML = "Host"
        const speakerName = document.createElement('h1');
        speakerName.classList.add(`${host && 'host-name' || 'speaker-name'}`);
        speakerName.innerHTML = name;
    
        const speakerProf = document.createElement('span');
        profession && speakerProf.classList.add(`${host && 'host-prof' || 'speaker-prof'}`);
        speakerProf.innerHTML = profession || '';
    
    
        // const hostAnimationWorkPlace = document.createElement('span');
        // hostAnimationWorkPlace.innerHTML = profession && profession;
    
        host &&  speakerBio.appendChild(hostAnimationTitle);
        
        speakerBio.appendChild(speakerName);
        speakerBio.appendChild(speakerProf);
        // host &&  speakerBio.appendChild(hostAnimationWorkPlace);
    
    
        const speakerPitch = document.createElement('div');
        speakerPitch.classList.add('speaker-pitch');
    
    
        speakerPitchTitle = document.createElement('div');
        speakerPitchTitle.classList.add('speaker-pitch-title');
        speakerPitchTitle.innerHTML = title;
    
        const speakerPitchTime = document.createElement('div');
            speakerPitchTime.classList.add('speaker-pitch-time');
    
            const speakerPitchTimeIcon = document.createElement('i');
            speakerPitchTimeIcon.classList.add('far', 'fa-clock');
    
            const speakerPitchTimeHour = document.createElement('span');
            speakerPitchTimeHour.innerHTML = time
    
        speakerPitchTime.appendChild(speakerPitchTimeIcon);
        speakerPitchTime.appendChild(speakerPitchTimeHour)
    
        const speakerPitchPlace = document.createElement('div');
            speakerPitchPlace.classList.add('speaker-pitch-place');
    
            const speakerPitchPlaceIcon = document.createElement('i');
            speakerPitchPlaceIcon.classList.add('far', 'fa-calendar-check');
    
            const speakerPitchPlaceHour = document.createElement('span');
            speakerPitchPlaceHour.innerHTML = place;
    
        speakerPitchPlace.appendChild(speakerPitchPlaceIcon);
        speakerPitchPlace.appendChild(speakerPitchPlaceHour)
    
    
         const speakerPitchDate = document.createElement('div');
            speakerPitchDate.classList.add('speaker-pitch-date');
    
            const speakerPitchDateIcon = document.createElement('i');
            speakerPitchDateIcon.classList.add('fas', 'fa-map-marker-alt');
    
            const speakerPitchDateHour = document.createElement('span');
            speakerPitchDateHour.innerHTML = pitchDate;
    
        speakerPitchDate.appendChild(speakerPitchDateIcon);
        speakerPitchDate.appendChild(speakerPitchDateHour)
    
        speakerPitch.appendChild(speakerPitchTime);
        speakerPitch.appendChild(speakerPitchPlace);
        speakerPitch.appendChild(speakerPitchDate);
    
    
        const hostGlow = document.createElement('div');
        hostGlow.classList.add('glow-line');
    
    
        !host && speakerSingle.appendChild(speakerCardOverlay);
        speakerSingle.appendChild(speakerImage);
        speakerSingle.appendChild(speakerBio);
        speakerSingle.appendChild(speakerBio);
        host && speakerSingle.appendChild(hostGlow);
        host && speakerSingle.appendChild(becomeASpeakerBtn);
        !host && speakerSingle.appendChild(speakerPitch);
        
        speakersContainer.appendChild(speakerSingle)
    });
    
    
    overlayCloseBtn.addEventListener('click', () => {
        const currentElem = document.querySelector('.speaker-single.collapsed') || document.querySelector('.gca--form-submission-wrapper.collapsed');
      
        popupToggler(currentElem, overlay, true)
    });
}