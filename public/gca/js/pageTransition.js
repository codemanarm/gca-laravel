const headerNavs = [...document.querySelectorAll('.header_nav_item')];
const headerLogo = document.querySelector('.header_logo');
let activeHeader;


const changeRouteNameForHistoryAPI = (homepageRoute, navigationElements) =>{
    const getRouteName = location.pathname.replace('/','').replace('.html','');

    if(getRouteName == homepageRoute) return;

    let getCurrentNavigationItem = navigationElements.find(item => {
        return item.dataset.navigation === getRouteName
    });

    getCurrentNavigationItem && getCurrentNavigationItem.classList.add('header_nav_item-active')
    activeHeader = getCurrentNavigationItem
}


headerLogo.addEventListener('click',() => {
    activeHeader && activeHeader.classList.remove('header_nav_item-active');
})

headerNavs.forEach(item => {
    item.addEventListener('click', (e) => {
        if(activeHeader) activeHeader.classList.remove('header_nav_item-active');

        e.target.classList.add('header_nav_item-active');
        activeHeader = e.target;
    })
})


window.addEventListener('load', () => {
    let success = document.querySelector('.allert.success.email-subscribe-form');
    if(success){
        let successClose = document.createElement('button');
        successClose.innerHTML = "X";
        successClose.classList.add('successClose')
        success.appendChild(successClose);

        successClose.addEventListener('click',() => {
            success.parentNode.removeChild(success);
        })
    }

    changeRouteNameForHistoryAPI('home', headerNavs);
    let isBurgerOpen = false;
    let burger = document.querySelector('.burger_container');
    let burgerBarContainer = document.querySelector('.burger_bar_container');

    if(document.querySelector('body.home') !== null){
        typeof window.particleStart == "function" && window.particleStart();
        typeof window.glitchText == "function" && window.glitchText();
        

       

        swiper = new Swiper('.swiper-container', {
            spaceBetween: 50,
            centeredSlides: true,
            slidesPerView: 1,
            loop: true,
            autoplay: {
              delay: 4500,
              disableOnInteraction: false,
            },
        });
    }
    const handleBurgerClick = (target) => {
        console.log(target);
        
        if(!isBurgerOpen) target.classList.add('active');

        if(isBurgerOpen) target.classList.remove('active');

        isBurgerOpen = !isBurgerOpen;
    }
    
    burger.addEventListener('click', function(){
        handleBurgerClick(burgerBarContainer);
    })

    if(document.querySelector('.sponsors_page') !== null){
        initFormPopupEvents('#sponsors_container .gca--open_form', '#gca--form-submissionSponsor');
        initFormPopupEvents('.partner_section_header .gca--open_form', '#gca--form-submissionPartner');
    }
})


var swiper;
function popupToggler(element,overlay,closeBtn){
    if(closeBtn){
        element.classList.remove('collapsed');
        overlay.classList.remove('show');
        document.body.classList.remove('collapsed');
        return;
    }
    
    element.classList.add('collapsed');
    overlay.classList.add('show');
    document.body.classList.add('collapsed');
};

function initFormPopupEvents(formButtonClassName, formContainer){
    const sponsorForm = document.querySelector(formContainer);
    
    const formBtn = document.querySelector(`${formButtonClassName}`);
    const overlay = document.querySelector('.overlay');
    const overlayCloseBtn = document.querySelector('.close_button');

    formBtn.addEventListener('click', () => {
        popupToggler(sponsorForm, overlay)
    });

    overlayCloseBtn.addEventListener('click', () => {
        const currentElem = document.querySelector('.gca--form-submission-wrapper.collapsed');
        popupToggler(currentElem, overlay, true)
    });
}

