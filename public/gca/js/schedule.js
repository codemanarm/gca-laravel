const initSchedule = () => {
  const schedule = [
    {
      start: "09:00",
      end: "10:00",
      hall: 1,
      type: "art",
      title: "Registration",
      description: ""
    },
    {
      start: "10:00",
      end: "10:30",
      hall: 1,
      type: "development",
      title: "Opening ceremony",
      description: ""
    },
    {
      start: "10:30",
      end: "13:30",
      hall: 1,
      type: "game design",
      title: "Coming Soon",
      description: ""
    },
    {
      start: "10:30",
      end: "13:30",
      hall: 2,
      type: "art",
      title: "Coming Soon",
      description: ""
    },
    {
      start: "10:30",
      end: "13:30",
      hall: 3,
      type: "development",
      title: "Coming Soon",
      description: ""
    },
   
  ];
  
  const scheduleWrapper = document.querySelector(".schedule_row_wrapper");
  const getMobileScheduleRow = document.querySelectorAll('.schedule_row_mob');
  function getGameCategory(category) {
    switch (category) {
      case "game design":
        return "design";
        break;
      case "art":
        return "art";
        break;
      case "development":
        return "development";
        break;
      default:
        return "other";
        break;
    }
  }

  function getGameColors(category){
    switch (getGameCategory(category)) {
      case "design":
        return "#bb6600";
        break;
      case "art":
        return "#7a0026";
        break;
      case "development":
        return "#007b11";
        break;
      default:
        return "#615c60";
        break;
    }
  }
  
  function timeToDecimal(t) {
    var arr = t.split(":");
    var dec = parseInt((arr[1] / 6) * 10, 10);
    return parseFloat(parseInt(arr[0], 10) + "." + (dec < 10 ? "0" : "") + dec);
  }
  
  function createEvent(event) {
    
    if(window.innerWidth > 1200){
      let first = 9;
      let div = document.createElement("div");
      let divTitle = document.createElement("h3");
      let divDescription = document.createElement("p");
      let divTime = document.createElement("span");
    
      let startTimeDecimal = timeToDecimal(event.start);
      let endTimeDecimal = timeToDecimal(event.end);
      divTitle.innerHTML = event.title;
      divDescription.innerHTML = event.description;
      divTime.innerHTML = event.start + "-" + event.end;
    
      div.appendChild(divTitle);
      div.appendChild(divDescription);
      div.appendChild(divTime);
    
      div.className =
        "schedule_row_event schedule_row_event_" + getGameCategory(event.type);
      div.style.top = (event.hall - 1) * 100 + 40 + 1 + "px";
      div.style.left = (startTimeDecimal - first) * 300 + 1 + "px";
      div.style.width = (endTimeDecimal - startTimeDecimal) * 300 - 2 + "px";

      scheduleWrapper.appendChild(div);
    }

    if(window.innerWidth <  1199){
      div.style.setProperty( `--${getGameCategory(event.type)}`, `${getGameColors(event.type)}`);
    }
  }
  
  for (let i = 0; i < schedule.length; i++) {
    createEvent(schedule[i]);
  }
  
  scheduleWrapper.addEventListener("wheel", function(e) {
    let speed = 0.5;
    e.preventDefault();
    scheduleWrapper.scrollLeft += e.deltaY * speed;
  });
  
}